import decimal
import os
import unittest

from discount.common import all_keys_exist

from discount.shipping_price_reading import data_to_pricing_table,\
    read_shipping_price_file
from discount.exceptions import PricingDataMissingKeysException


class TestAllKeysExist(unittest.TestCase):

    def setUp(self):
        self.d = {'a': 1, 'b': 2}

    def test_all_keys(self):
        rez = all_keys_exist(self.d, ['a', 'b'])
        self.assertTrue(rez)

    def test_one_key(self):
        rez = all_keys_exist(self.d, ['a'])
        self.assertTrue(rez)

    def test_empty_list(self):
        rez = all_keys_exist(self.d, [])
        self.assertTrue(rez)

    def test_not_existing_key(self):
        rez = all_keys_exist(self.d, ['c'])
        self.assertFalse(rez)


class TestDataToPricingTable(unittest.TestCase):

    def setUp(self):
        self.data_sample = [
            {
                "provider": "LP",
                "package_size": "M",
                "price": "4.90",
                "currency": "eur"
            },
            {
                "provider": "LP",
                "package_size": "L",
                "price": "6.90",
                "currency": "eur"
            }
        ]

    def test_without_exception(self):
        pricing_table = data_to_pricing_table(self.data_sample)
        self.assertEqual(len(pricing_table), 2)
        self.assertEqual(type(pricing_table[0]['price']), decimal.Decimal)
        self.assertEqual(type(pricing_table[1]['price']), decimal.Decimal)
        self.assertEqual(len(pricing_table[1]), 4)

    def test_data_missing_keys(self):
        del self.data_sample[0]['currency']
        self.assertRaises(PricingDataMissingKeysException,
                          data_to_pricing_table, self.data_sample)


class TestReadShippingPriceFile(unittest.TestCase):

    def get_prices_file(self):
        directory = os.path.dirname(os.path.abspath(__file__))
        return os.path.join(directory, '..', 'discount', 'prices.json')

    def test_file_reading(self):
        file_name = self.get_prices_file()
        pricing_table = read_shipping_price_file(file_name)
        self.assertEqual(len(pricing_table), 6)

if __name__ == '__main__':
    unittest.main()
