import decimal
import unittest

from discount.common import filter_table_by_attribute_values
from discount.shipping_price_quick_lookup import generate_pricing_dictionary, \
    get_lowest_prices_for_size
from discount.exceptions import DuplicatePriceException


class TestGeneratePricingDictionary(unittest.TestCase):

    def setUp(self):
        self.demo_table = [
            {
                "provider": "LP",
                "package_size": "M",
                "price": decimal.Decimal("1.50"),
                "currency": "usd"
            },
            {
                "provider": "LP",
                "package_size": "M",
                "price": decimal.Decimal("4.90"),
                "currency": "eur"
            },
            {
                "provider": "LP",
                "package_size": "L",
                "price": decimal.Decimal("6.90"),
                "currency": "eur"
            },
            {
                "provider": "MR",
                "package_size": "S",
                "price": decimal.Decimal("2"),
                "currency": "eur"
            },
        ]

    def test_empty_pricing_table(self):
        rez = generate_pricing_dictionary([])
        self.assertDictEqual(rez, {})

    def test_one_line_pricing_table(self):
        list_with_one_dict = [self.demo_table[0]]
        rez = generate_pricing_dictionary(list_with_one_dict)
        expected = {
            'LP': {
                'M': {'usd': decimal.Decimal('1.50')}
            }
        }
        self.assertDictEqual(rez, expected)

    def test_multiple_lines_pricing_table(self):
        rez = generate_pricing_dictionary(self.demo_table)
        expected = {
            'LP': {
                'M': {'usd': decimal.Decimal('1.50'),
                      'eur': decimal.Decimal('4.90')},
                'L': {'eur': decimal.Decimal('6.90')}
            },
            'MR': {
                'S': {'eur': decimal.Decimal('2')}
            }
        }
        self.assertDictEqual(rez, expected)

    def test_duplicate_entries(self):
        self.demo_table[2] = self.demo_table[1]
        self.assertRaises(DuplicatePriceException, generate_pricing_dictionary,
                          self.demo_table)


class TestFilterTableByAttributeValue(unittest.TestCase):

    def test_empty_list(self):
        rez = filter_table_by_attribute_values([], 'a', [1, 2, 3])
        self.assertListEqual(list(rez), [])

    def test_empty_values(self):
        rez = filter_table_by_attribute_values([{'a': 1}], 'a', [])
        self.assertListEqual(list(rez), [])

    def test_mutiple_items_multiple_values(self):
        table = [
            {'a': 1, 'b': 2, 'z': 1},
            {'a': 2, 'c': 2, 'z': 2},
            {'a': 2, 'c': 1, 'z': 3},
            {'d': 1, 'c': 0, 'z': 4},
            {'a': 2, 'c': 2, 'z': 2},
        ]

        expected = [
            {'a': 2, 'c': 2, 'z': 2},
            {'a': 2, 'c': 1, 'z': 3},
            {'a': 2, 'c': 2, 'z': 2},
        ]

        rez = filter_table_by_attribute_values(table, 'c', [1, 2])
        self.assertListEqual(list(rez), expected)


class TestGetLowestPricesForSize(unittest.TestCase):

    def setUp(self):
        self.demo_table = [
            {
                "provider": "LP",
                "package_size": "M",
                "price": decimal.Decimal("1.50"),
                "currency": "usd"
            },
            {
                "provider": "LP",
                "package_size": "M",
                "price": decimal.Decimal("4.90"),
                "currency": "eur"
            },
            {
                "provider": "MR",
                "package_size": "L",
                "price": decimal.Decimal("6.90"),
                "currency": "eur"
            },
            {
                "provider": "MR",
                "package_size": "M",
                "price": decimal.Decimal("1.4"),
                "currency": "usd"
            },
            {
                "provider": "MR",
                "package_size": "M",
                "price": decimal.Decimal("5.01"),
                "currency": "eur"
            },
        ]

    def test_get_lowest_prices_for_size(self):
        rez = get_lowest_prices_for_size(self.demo_table, 'M')
        expected = {
            'usd': decimal.Decimal("1.4"),
            'eur': decimal.Decimal("4.9"),
        }
        self.assertDictEqual(rez, expected)
