#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='DiscountApp',
    version='0.1dev',
    url='https://bitbucket.org/kostast/discountapp',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'vdiscount = discount.application:main',
        ]
    },
    author='Kostas Tamošiūnas',
    author_email='tamosiunas@gmail.com',
    description='Shipment discount calculation module',
    long_description=open('README.txt').read(),
    test_suite="tests",
)
