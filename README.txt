Shipment discount calculation module
====================================

Description and requirements
https://gist.github.com/raimondasv/4dce3ec02ff1e1568361

Installation/Deployment
=======================

Instalation
-----------

User instalation:

``python3 setup.py install``


Developer instalation:

``pip install -e .``

Now ``vdiscount`` command should be available in your shell.

Running application
-----------------------------
Solution is made having python3.3+ in mind.

Launching application manual way form source folder:

``vdiscount``

Or by using command line parameters:

``vdiscount --input_file some_input.txt --json_parcel prices.json --monthly_limit 8.3``


Automatic testing
-----------------
There is tool which can manage environments during testing::

  pip install tox
  tox -l # lists environments
  tox -e py33 #will launch testing and coverage on python 3.3
  tox # will execute testing on python 3.3 and 3.4. Will launch flake8(lint tools) on python 3.4.

Do not forget to install corresponding python versions.


Design solution
===============

**discount/application.py : main()**

Application main entry point

**discount/read_shipping_price_file.py : read_shipping_price_file()**

Reads prices file. Returns::

  [
     {"provider": '..', "package_size": '..', "price": '..', "currency": '..'},
     {"provider": '..', "package_size": '..', "price": '..', "currency": '..'}
  ]

**discount/shipping_price_quick_lookup : generate_pricing_dictionary()**

From read_shipping_price_file() output constructs data structure for quick
lookup of pricing. Example of data structure::

  {
    'LP': {
        'M': {'usd': decimal.Decimal('1.50'),
              'eur': decimal.Decimal('4.90')},
        'L': {'eur': decimal.Decimal('6.90')}
    },
    'MR': {
        'S': {'eur': decimal.Decimal('2')}
    }
  }

**discount/shipping_price_quick_lookup : get_lowest_prices_for_size()**

Finds lowest price for given size. Groups them by currency. Example::

    {
        'usd': decimal.Decimal("1.4"),
        'eur': decimal.Decimal("4.9"),
    }

**discount/input_reader.py : parse_input_file()**

Reads given input file. Return given data structure::

    [
        {'line': line, 'date': date, 'provider': provider, 'size': size},
        {'line': line, 'date': date, 'provider': provider, 'size': size}
        ..
    ]

When parsing line failed - date, provider and size fields will be
set to None. Dictionaries having this structure::

   {'line': line, 'date': date, 'provider': provider, 'size': size},

will be called ``dline``.


**discount/suggest_discount:**

Modules takes dline. Adds fields with suggested discount.

**discount/main.py:**

Here goes all processing. ``calculate_discounts`` responsible for
managing discount calculation. It sorts input, asks for discount suggestion,
applies monthly limits and returns dlines with calculated discounts.

**discount/writer.py**

Responsible for output generation.