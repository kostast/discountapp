import copy


def suggest_lowest_price_discount(d, currency_price, currency, size='S'):
    d = copy.deepcopy(d)
    if d.get('size') == size:
        d['suggested_discount'] = d['default_price'] - currency_price[currency]
    return d


class suggest_nth_free():

    def __init__(self, size='L', provider='LP', nth=3):
        self.size = size
        self.provider = provider
        self.nth = nth
        self.yearmonth = None
        self.counter = 0

    def __call__(self, d):
        if d.get('date') is None:
            return d
        if self.yearmonth != d['date'].timetuple()[:2]:
            self.yearmonth = d['date'].timetuple()[:2]
            self.counter = 0
        if d['size'] == self.size and self.provider == d['provider']:
            self.counter += 1

        if self.counter == self.nth:
            d = copy.deepcopy(d)
            d['suggested_discount'] = d['default_price']
        return d
