# -*- coding: utf-8 -*-

"""
Discountapp exceptions
"""


class PricingDataMissingKeysException(Exception):
    """
    Raised when pricing data source is missing keys in pricing information.
    """
    def __init__(self, expected_keys):
        super().__init__()
        self.expected_keys = expected_keys

    def __str__(self):
        return "Not all of expected keys was (%s) was found." % \
            self.expected_keys


class DuplicatePriceException(Exception):
    """
    Raised when there are given multiple prices for same provider, package
    sizes and currency.
    """
    def __init__(self, provider, package_size, currency):
        super().__init__()
        self.provider = provider
        self.package_size = package_size
        self.currency = currency

    def __str__(self):
        return "Multiple prices for provider '%s', pacakage size '%s' and " \
            "currency '%s'." % (self.provider, self.package_size,
                                self.currency)
