#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions for reading and validating pricing files.
"""

import copy
import json

from decimal import Decimal

from discount.common import all_keys_exist
from discount.exceptions import PricingDataMissingKeysException


def read_json_file(file_name):
    """
    Reads JSON file with given file_name and returns decoded data structure.
    """
    with open(file_name) as f:
        return json.load(f)


def data_to_pricing_table(data):
    """
    Reads data stream. Returns list of dictionaries where each dictionary
    contains keys: "provider", "package_size", "price", "currency".
    Price field is converted to Decimal.
    """
    data = copy.deepcopy(data)
    required_keys = ["provider", "package_size", "price", "currency"]
    dictionary_validity = [all_keys_exist(d, required_keys) for d in data]
    if not(all(dictionary_validity)):
        raise PricingDataMissingKeysException(required_keys)
    for d in data:
        d['price'] = Decimal(d['price'])
    return data


def read_shipping_price_file(file_name):
    """
    Reads file 'file_name'. Returns list of dictionaries where each dictionary
    contains keys: "provider", "package_size", "price", "currency".
    Price field is converted to Decimal.
    """
    data = read_json_file(file_name)
    pricing_table = data_to_pricing_table(data)
    return pricing_table
