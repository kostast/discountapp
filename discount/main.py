
import decimal
import copy

from discount.shipping_price_quick_lookup import generate_pricing_dictionary, \
    get_lowest_prices_for_size
from discount.suggest_discount import suggest_lowest_price_discount, \
    suggest_nth_free


def sort_file_by_date(dlines):
    return sorted(dlines, key=lambda d: d['line'][:10])


def set_default_price(d, pricing_dictonary, currency):
    d = copy.deepcopy(d)
    if d.get('provider') and d.get('size'):
        provider, size = d['provider'], d['size']
        d['default_price'] = pricing_dictonary[provider][size][currency]
    return d


def discount_montly_limit(dlines, montly_limit):
    last_month = None
    rez = []

    for dline in dlines:
        if not dline.get('date'):
            rez.append(dline)
            continue
        # check if calanedar month has changed
        if last_month != dline['date'].timetuple()[:2]:
            last_month = dline['date'].timetuple()[:2]
            this_month_limit = decimal.Decimal(montly_limit)

        if 'suggested_discount' not in dline:
            dline['suggested_discount'] = 0

        allowed_discount = min(this_month_limit, dline['suggested_discount'])
        dline['final_discount'] = allowed_discount
        dline['final_price'] = dline['default_price'] - allowed_discount
        this_month_limit -= allowed_discount
        rez.append(dline)
    return dlines


def calculate_discounts(dlines, pricing_table, montly_limit, currency='eur'):

    pricing_dictonary = generate_pricing_dictionary(pricing_table)
    currency_price = get_lowest_prices_for_size(pricing_table, size='S')

    dlines = sort_file_by_date(dlines)

    suggest_3rd_free = suggest_nth_free(size='L', provider='LP', nth=3)
    rez = []
    for dline in dlines:
        dline = set_default_price(dline, pricing_dictonary, currency)
        dline = suggest_lowest_price_discount(dline, currency_price,
                                              currency, size='S')
        dline = suggest_3rd_free(dline)
        rez.append(dline)
    return discount_montly_limit(rez, montly_limit)
