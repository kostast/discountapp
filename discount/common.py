def all_keys_exist(d, keys):
    """
    Retuns True if dictionary 'd' has all keys in 'keys'. Otherwise returns
    False.
    """
    for key in keys:
        if key not in d:
            return False
    return True


def filter_table_by_attribute_values(dict_table, key, values):
    """
    Given json_table - list of dictionaries. This filter returns list of
    dictionaries that has key 'key' and value of dictionary[key] is in 'values'
    list.
    """
    return filter(lambda item: key in item and item[key] in values, dict_table)
