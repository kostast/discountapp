import io


def generate_output(dlines):
    f = io.StringIO()

    for d in dlines:
        if d.get('date'):
            if d['final_discount'] > 0:
                formatted_line = "%s %s %s\n" % (
                    d['line'], d['final_price'], d['final_discount'])
            else:
                formatted_line = "%s %.2f %s\n" % (
                    d['line'], d['final_price'], '-')
        else:
            formatted_line = "%s %s\n" % (d['line'], 'Ignored')
        f.write(formatted_line)
    return f.getvalue()
