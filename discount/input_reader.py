import datetime


def read_input_file(file_name):
    with open(file_name) as f:
        lines = f.readlines()
        return lines


def process_line(line, providers, sizes, date_format='%Y-%m-%d'):
    """
    Returns dictionary. Result always has key *line* with value of a line.
    *date*, *provider*, *size* - all of them be set to parsed values or all
    of them will be set to None.
    """

    try:
        line = line.strip()
        raw_date, raw_package_size, raw_provider = line.split()
        date = datetime.datetime.strptime(raw_date, date_format).date()
        provider = raw_provider if raw_provider in providers else None
        size = raw_package_size if raw_package_size in sizes else None
    except ValueError:
        provider = None
        size = None
        date = None
    return {'line': line, 'date': date, 'provider': provider, 'size': size}


def processes_input_lines(lines, providers, sizes):
    """
    Processes each line with `process_line` function. Returns list of
    processed objects.
    """

    return [process_line(line, providers, sizes) for line in lines]


def parse_input_file(file_name, providers, sizes):
    lines = read_input_file(file_name)
    return processes_input_lines(lines, providers, sizes)
