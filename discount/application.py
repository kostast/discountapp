# coding=utf-8

import argparse
import decimal
import os
import sys

from decimal import Decimal


from discount.input_reader import parse_input_file
from discount.main import calculate_discounts
from discount.shipping_price_reading import read_shipping_price_file
from discount.writer import generate_output


def get_parser():
    directory = os.path.dirname(os.path.abspath(__file__))
    prices_json = os.path.join(directory, 'prices.json')
    parser = argparse.ArgumentParser(description='Shipment discount '
                                                 'calculation.')
    parser.add_argument('--input_file', default='input.txt',
                        help='File containing list of transactions')
    parser.add_argument('--json_parcels', default=prices_json,
                        help='Parcel prices file')
    parser.add_argument('--monthly_limit', default='10',
                        help='Monthly discount limit')
    return parser


def main():

    try:
        args = get_parser().parse_args()
        pricing_table = read_shipping_price_file(args.json_parcels)
        providers = set([d['provider'] for d in pricing_table])
        sizes = set([d['package_size'] for d in pricing_table])
        input_dicts = parse_input_file(args.input_file, providers, sizes)
        rez = calculate_discounts(input_dicts, pricing_table,
                                  Decimal(args.monthly_limit), currency='eur',)
        sys.stdout.write(generate_output(rez))
    except FileNotFoundError as e:  # NOQA
        sys.stderr.write("ERROR: File '%s' not found\n" % e.filename)
    except decimal.InvalidOperation as e:
        sys.stderr.write("Error: Invalid decimal value for monthly limit\n")
    except Exception as e:
        sys.stderr.write("ERROR: NO DETAIED INFORMATION AVAILABLE. " + str(e))

if __name__ == '__main__':
    main()
