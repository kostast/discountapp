# -*- coding: utf-8 -*-

"""
Parses pricing table in various ways to generate data structures for quick
search and lookup.
"""

import collections
from discount.common import filter_table_by_attribute_values

from discount.exceptions import DuplicatePriceException


def generate_pricing_dictionary(pricing_table):
    """
    From pricing_table (format described in data_to_pricing_table() ) generates
    dictionary with following structure:
    {
        'provider_name':{
            'package_size':{
                currency1: price1,
                currency2: price2,
                ...
            }
            ...
        }
        ...
    }
    """

    pricing = {}
    for row in pricing_table:
        add_pricing_information(
            pricing,
            row['provider'],
            row['package_size'],
            row['currency'],
            row['price'])
    return pricing


def add_pricing_information(pricing, provider, package_size, currency, price):
    """
    Adds pricing information into dictionary pricing to meet data stucture
    defined in 'generate_pricing_dictionary'.
    """
    package_sizes = pricing.get(provider, {})
    currencies = package_sizes.get(package_size, {})

    # Problem if we have multiple prices for same shipment provider, package
    # size, currency
    if currency in currencies:
        raise DuplicatePriceException(package_size, price, currency)
    currencies[currency] = price

    package_sizes[package_size] = currencies
    pricing[provider] = package_sizes


def get_lowest_prices_for_size(pricing_table, size='S'):
    """
    Takes pricing table and filters out by size. Returns dictionary with
    lowest price by currency. E.g. {'eur': Decimal(5.32), 'usd': Decimal(4.1)}.
    """
    specific_size = filter_table_by_attribute_values(
        pricing_table, 'package_size', [size])
    group_prices_by_currency = collections.defaultdict(list)
    for row in specific_size:
        price = row['price']
        currency = row['currency']
        group_prices_by_currency[currency].append(price)
    currency_price = {currency: min(prices)
                      for currency, prices in group_prices_by_currency.items()}
    return currency_price
